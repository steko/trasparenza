"""
This script is intended for use on a specific web application.

The web application is a data entry frontend based on WordPress and Gravity Forms.

The script logic is however generic enough that it could be useful in other cases.
"""

import time
from csv import DictReader

from trasparenza import html_login
from settings import SETTINGS


def load_csv(csvfile="provvedimenti.csv"):
    with open(csvfile, newline="", encoding="utf-8") as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            input_13 = (
                (
                    "input_13",
                    (row["input_13"], open(row["input_13"], "rb"), "application/pdf"),
                ),
            )
            del row["input_13"]
            payload_csv_step1 = tuple([(i, (None, j)) for i, j in row.items()])
            payload_csv = payload_csv_step1 + input_13
            yield payload_csv


def submit_provvedimento(session, url):
    r = session.get("{url}/?p=337".format(**SETTINGS))
    r.html.render()

    _gform_submit_nonce_22 = r.text[
        r.text.find("_gform_submit_nonce_22")
        + 61 : r.text.find("_gform_submit_nonce_22")
        + 71
    ]
    gform_ajax = "form_id=22&title=&description=&tabindex=1"
    _wp_http_referer = "/?p=337"
    is_submit_22 = "1"
    gform_submit = "22"
    gform_unique_id = ""
    state_22 = r.text[r.text.find("state_22") + 17 : r.text.find("state_22") + 73]
    gform_target_page_number_22 = "0"
    gform_source_page_number_22 = "1"
    gform_field_values = ""

    hidden_payload = (
        ("gform_ajax", (None, gform_ajax)),
        ("_gform_submit_nonce_22", (None, _gform_submit_nonce_22)),
        ("_wp_http_referer", (None, _wp_http_referer)),
        ("is_submit_22", (None, is_submit_22)),
        ("gform_submit", (None, gform_submit)),
        ("gform_unique_id", (None, gform_unique_id)),
        ("state_22", (None, state_22)),
        ("gform_target_page_number_22", (None, gform_target_page_number_22)),
        ("gform_source_page_number_22", (None, gform_source_page_number_22)),
        ("gform_field_values", (None, gform_field_values)),
    )

    for csv_payload in load_csv():
        payload = csv_payload + hidden_payload
        print("UPLOADING {}".format(payload[5][1][1]))
        # the files parameter is used to ensure the data is sent as multipart/form-data as required
        response = session.post("{url}/?p=337#gf_22".format(**SETTINGS), files=payload)
        print(response.text)
        print("UPLOADED {}".format(payload[5][1][1]))
        nap = 15  # seconds to sleep
        print("Sleeping for {} seconds ...".format(nap))
        time.sleep(nap)


if __name__ == "__main__":
    session = html_login(SETTINGS)
    submit_provvedimento(session, SETTINGS)
