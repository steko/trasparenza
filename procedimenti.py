"""
This script is intended for use on a specific web application.

The web application is a data entry frontend based on WordPress and Gravity Forms.

The script logic is however generic enough that it could be useful in other cases.
"""

import time
from csv import DictReader

from trasparenza import html_login
from settings import SETTINGS


def load_csv(csvfile="procedimenti.csv"):
    with open(csvfile, newline="", encoding="utf-8") as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            input_8 = tuple(
                [("input_8[]", (None, i)) for i in row["input_8[]"].split("/")]
            )
            del row["input_8[]"]
            input_24 = tuple(
                [("input_24[]", (None, i)) for i in row["input_24[]"].split("/")]
            )
            del row["input_24[]"]
            payload_csv_step1 = tuple([(i, (None, j)) for i, j in row.items()])
            payload_csv = (
                payload_csv_step1[0:5] + input_8 + input_24 + payload_csv_step1[5:]
            )
            yield payload_csv


def submit_proc(session, url):
    r = session.get("{url}/?p=172".format(**SETTINGS))
    r.html.render()

    _gform_submit_nonce_15 = r.text[
        r.text.find("_gform_submit_nonce_15")
        + 61 : r.text.find("_gform_submit_nonce_15")
        + 71
    ]
    gform_ajax = "form_id=15&title=&description=&tabindex=1"
    _wp_http_referer = "/?p=172"
    is_submit_15 = "1"
    gform_submit = "15"
    gform_unique_id = ""
    state_15 = r.text[r.text.find("state_15") + 17 : r.text.find("state_15") + 73]
    gform_target_page_number_15 = "0"
    gform_source_page_number_15 = "1"
    gform_field_values = ""

    hidden_payload = (
        ("gform_ajax", (None, gform_ajax)),
        ("_gform_submit_nonce_15", (None, _gform_submit_nonce_15)),
        ("_wp_http_referer", (None, _wp_http_referer)),
        ("is_submit_15", (None, is_submit_15)),
        ("gform_submit", (None, gform_submit)),
        ("gform_unique_id", (None, gform_unique_id)),
        ("state_15", (None, state_15)),
        ("gform_target_page_number_15", (None, gform_target_page_number_15)),
        ("gform_source_page_number_15", (None, gform_source_page_number_15)),
        ("gform_field_values", (None, gform_field_values)),
    )

    for csv_payload in load_csv():
        payload = csv_payload + hidden_payload
        print("UPLOADING {}".format(payload[3][1][1]))
        # the files parameter is used to ensure the data is sent as multipart/form-data as required
        r = session.post("{url}/?p=172#gf_15".format(**SETTINGS), files=payload)
        print("UPLOADED {}".format(payload[3][1][1]))
        nap = 15 # seconds to sleep
        print("Sleeping for {} seconds ...".format(nap))
        time.sleep(nap)

if __name__ == "__main__":
    session = html_login(SETTINGS)
    submit_proc(session, SETTINGS)
