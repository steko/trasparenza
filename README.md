# Trasparenza

This script is intended for use on a specific web application at the Ministry of Culture of Italy.

The web application is a data entry frontend based on WordPress and Gravity Forms.

The script logic is however generic enough that it could be useful in other cases.

## Quickstart

This collection of scripts helps with the task of data entry for compliance to public administration laws concerning transparency.

Create a virtual environment and install the requirements with pip:

```
python -m venv env
source env/bin/activate # or similar command for Windows
pip install -r requirements.txt
```

Create a file called `settings.py` in the same directory of the script, with content like this:

```
SETTINGS = {
    "url": "https://example.com", # base url of website, no trailing slash
    "log": "", # username
    "pwd": "", # password
}
```

This file is included in the `.gitignore` for the repository.

## Procedimenti

Fill the CSV file (by default `procedimenti.csv`) with the tool of your choice. The default encoding is UTF-8.

Run the script with:

```
python procedimenti.py
```

## Provvedimenti

Fill the CSV file (by default `provvedimenti.csv`) with the tool of your choice. The default encoding is UTF-8.

This script uploads text data and one PDF file for each record. The CSV field (column) must contain the path of the PDF file relative to the directory where the script is run.

Run the script with:

```
python provvedimenti.py
```
