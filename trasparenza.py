"""
This module is part of the 'trasparenza' script.

It contains code that is shared by other modules.
"""

from requests_html import HTMLSession


def html_login(SETTINGS):
    with HTMLSession() as session:
        response = session.get("{url}/wp-login.php".format(**SETTINGS))
        sess_id = r.html.find("input[name=sess_id]", first=True).attrs["value"]
        payload = {
            "log": SETTINGS["log"],
            "pwd": SETTINGS["pwd"],
            "submit": "Login",
            "testcookie": "1",
            "session": sess_id,
        }
        response = session.post("{url}/wp-login.php".format(**SETTINGS), data=payload)
        return session

